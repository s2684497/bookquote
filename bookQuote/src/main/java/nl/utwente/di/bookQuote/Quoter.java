package nl.utwente.di.bookQuote;

import java.util.HashMap;
import java.util.Objects;

public class Quoter {
    HashMap<String, Double> bookPrices = new HashMap<>();
    public Quoter(){
        bookPrices.put("1", 10.0);
        bookPrices.put("2", 45.0);
        bookPrices.put("3", 20.0);
        bookPrices.put("4", 50.0);
        bookPrices.put("5", 10.0);
        bookPrices.put("others", 0.0);
    }


    public double getBookPrice(String isbn){
        for (String key : bookPrices.keySet()){
            if (key.equals(isbn)){
                return bookPrices.get(key);
            }
        }
        return bookPrices.get("others");
    }

}
